import './App.css';
import Footer from './Components/Footer';
import Header from './Components/Header';
import Products from './Components/Products';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import React, { Component } from 'react';
import EachProduct from './Components/EachProduct';
import axios from 'axios';
import AddProduct from './Components/AddProduct';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      product: [],
      loadingStatus: true,
      error: false,
      productFound: false,
      clickId: window.location.pathname
    }
  }

  componentDidMount() {
    
    axios.get(`https://fakestoreapi.com/products/${this.state.clickId}`)
      .then(response => {
        console.log("api is loaded");
        this.setState({
          product: response.data,
          loadingStatus: false,
          error: false,
          productFound: true
        });

      })
      .catch((error) => {
        this.setState({
          error: true,
          loadingStatus: false,
          productFound: false
        })
      })

  }
  createProduct = (newProduct) => {
    
    let stateProduct = [...this.state.product]
    console.log(stateProduct);
    console.log(...this.state.product);
    newProduct.id = this.state.product.length + 1
    stateProduct.push(newProduct)

    this.setState({
      product: stateProduct
    })

    console.log(this.state.product);
  }



  render() {
    return (
      <div className="App">
        

        <Routes>
          <Route path="/" element={<Products productData={this.state.product} loadingStatus={this.state.loadingStatus} error={this.state.error} productFound={this.state.productFound} />} />
          <Route path="/:id" element={<EachProduct id={this.state.id} />} />
          <Route path="addProduct" element={<AddProduct createProduct={this.createProduct} />} />

        </Routes>


        <Footer />
      </div>
    );

  }
}

export default App;













