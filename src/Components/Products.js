

import React from 'react';
import Card from './Card';
import Header from './Header';
import "./Products.css";




const Products = (props) => {
    
    const productData = (
        props.productData.length > 0
            ?
            props.productData.map((data) => {
                
                return (

                    <Card
                        key={data.id}
                        id={data.id}
                        title={data.title.slice(0, 60)}
                        category={data.category}
                        image={data.image}
                        price={data.price}
                        rating={data.rating.rate}
                    />
                )
            })
            :
            <p>No product Found</p>

    )


    return (
        <>
        <Header button={"Add Product"} link={"/addProduct"}/>
        <div className="container">
            {props.loadingStatus && <div className="lds-dual-ring loader"></div>}
            {props.productFound && productData}

            {props.error ? <div className='error'>Something seems to be broken </div> : null}
        </div>
        </>
    );
}

export default Products;
