import React from 'react';
import "./Card.css";
import { Link } from "react-router-dom";



const Card = (props) => {

    return (

        <div className='card'>
            <h3>{props.category}</h3>
            <div className='description'>

                <h1 className='title'>{props.title}</h1>
                {props.image!==undefined? <img src={props.image} ></img> :<img src="https://cdn.codespeedy.com/wp-content/uploads/2019/03/Chrome-Broken-Image-Icon.png"></img>}
                
                {console.log(props.image)}
                

            </div>

            <div className="button">
                <a>Price: ${props.price}</a><a className="cart-btn" >Rating :&#11088; {props.rating}/5</a>
            </div>

            <Link to={`/${props.id}`}>
                <button onClick={props.productClick} value={props.id} className="details" >Details</button>
            </Link>

        </div >

    );
}

export default Card;
