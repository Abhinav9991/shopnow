import React, { Component } from 'react';
import './AddProduct.css';
import validator from 'validator';
import Header from './Header';



class AddProduct extends Component {
    constructor() {
        super();
        this.state = {
            title: "",
            description: "",
            category: "",
            rate: "",
            price: "",
            errors: {},
            added:false
        }
    }


    validate = () => {

        const { title, description, price, category, rate, } = this.state
        const errors = {}
        if (!validator.isAlpha(title)) {
            errors.title = "Please enter the title of the Product"

        }
        if (!validator.isAlpha(description)) {
            errors.description = "Please add product description"
        }
        if (!validator.isNumeric(price)) {
            errors.price = "Please add Price for the product"
        }

        if (!validator.isAlpha(category)) {
            errors.category = "Please add product category"
        }

        if (!validator.isNumeric(rate)) {
            errors.rate = "Please add product rating"
        }


        return Object.keys(errors).length === 0 ? null : errors

    }

    handelSubmit = (event) => {
        event.preventDefault()
        const errors = this.validate()
        this.setState({ errors: errors || {} })
        // const errorLength=Object.keys(errors).length;
        if (errors === null) {
            this.setState({
                added:true
            })
            const newProduct = {
                title: event.target.title.value,
                price: event.target.price.value,
                description: event.target.description.value,
                category: event.target.category.value,
                rating: {
                    rate: Number(event.target.rate.value),
                }

            }
            this.props.createProduct(newProduct)
        }



    }

    handleChange = (event) => {

        if (event.target.name === "title") {
            this.setState(
                { title: event.target.value }
            )
            // console.log(event.target.value);
        }
        if (event.target.name === "description") {
            this.setState(
                { description: event.target.value }
            )
        }

        if (event.target.name === "price") {
            this.setState(
                { price: event.target.value }
            )
            console.log(event.target.value);
        }

        if (event.target.name === "category") {
            this.setState(
                { category: event.target.value }
            )

        }
        if (event.target.name === "rate") {
            this.setState(
                { rate: event.target.value }
            )

        }


    }

    render() {
        
        console.log(this.state.errors===null);
        return (
            <>
            <Header button={"See the Products"} link={"/"}/>
                <form onSubmit={this.handelSubmit}>
                    <h1>Add product</h1>
                    
                    <input placeholder='Enter product title' type="text" id="title" name="title" onChange={this.handleChange} onFocus={this.handleChange} value={this.state.title} />
                    {this.state.errors.title && <div className='errorMessage'>{this.state.errors.title}</div>}

                    
                    <input placeholder='Product description' type="text" id="description" name="description" onChange={this.handleChange} onFocus={this.handleChange} value={this.state.description} />
                    {this.state.errors.description && <div className='errorMessage'>{this.state.errors.description}</div>}

                    
                    <input placeholder='Product price' type="number" id="price" name="price" onChange={this.handleChange} onFocus={this.handleChange} value={this.state.price} />
                    {this.state.errors.price && <div className='errorMessage'>{this.state.errors.price}</div>}

                    
                    <input placeholder='Product category' type="text" id="category" name="category" onChange={this.handleChange} onFocus={this.handleChange}  value={this.state.category} />
                    {this.state.errors.category && <div className='errorMessage'>{this.state.errors.category}</div>}

                    
                    <input placeholder='Product rating' type="number" id="rate" name="rate" onChange={this.handleChange} onFocus={this.handleChange} value={this.state.rate} />
                    {this.state.errors.rate && <div className='errorMessage'>{this.state.errors.rate}</div>}

                    <button className='addButton'>Add</button>

                    {this.state.added===true ? <div className='successMessage'>Product has been added to the List</div>:null}
                </form>
            </>

        );
    }
}

export default AddProduct;
