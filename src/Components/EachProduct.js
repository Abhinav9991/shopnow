
import axios from 'axios';
import React, { Component } from 'react';
import './EachProduct.css'
import Header from './Header';



class EachProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // id: props.id,
      product: {},
      loadingStatus: true,
      error: false,
      productFound: false,
      clickId: window.location.pathname
    }
    console.log(`https://fakestoreapi.com/products${this.state.clickId}`);
    console.log(this.state.clickId);
  }

  componentDidMount() {
    // const product=this.state.id
    axios.get(`https://fakestoreapi.com/products/${this.state.clickId}`)

      .then(response => {
        console.log(response.data);
        this.setState({
          product: response.data,
          loadingStatus: false,
          error: false,
          productFound: true
        });

      })
      .catch((error) => {
        this.setState({
          error: true,
          loadingStatus: false,
          productFound: false
        })
      })

  }



  render() {
    try {

      const productData = (


        (
          <>

            <div className='eachProduct' >
              <h1>{this.state.product.title}</h1>
              <div className='productDetails'>
                <img src={this.state.product.image} style={{ height: "300px", width: "300px" }}></img>
              
                <p className='productDescription'> <span>About the product:</span>{this.state.product.description}</p>
                <p> <span>Price</span> : ${this.state.product.price}</p>
                <p><span>Rating</span> : {this.state.product.rating.rate}/5</p>
              </div>

            </div>
          </>
        )



      )

      return (
        <>
          <Header button={"See the Products"} link={"/"} />
          <div>
            {this.state.loadingStatus && <div className="lds-dual-ring loader"></div>}
            {this.state.productFound && productData}

            {this.state.error ? <div className='error'>Something seems to be broken </div> : null}
          </div>
        </>
      );

    }
    catch (error) {
      return (
        <div className="container">
          {this.state.loadingStatus && <div className="lds-dual-ring loader"></div>}
          {this.state.error ? <div className='error'>Something seems to be broken </div> : null}
        </div>
      )
    }



  }

}

export default EachProduct;
