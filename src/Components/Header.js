import React from 'react';
import './Header.css';
import { Link } from "react-router-dom";

const Header = (props) => {

    return (
        <div className='header'>
            <div className='row'>
                <h3>Shop<span className="span">Now</span></h3>
                <Link to={props.link}>
                    <button className='addButton'>{props.button}</button>
                </Link>

            </div>
            <p>We have wide range of products from wearables to electronics</p>


        </div>
    );
}

export default Header;
